package com.gionjo.yannis_gkionis_cs_1.features.profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gionjo.yannis_gkionis_cs_1.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.profile_root,new ProfileFragment())
                .commit();
    }
}
