package com.gionjo.yannis_gkionis_cs_1.features.message;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gionjo.yannis_gkionis_cs_1.R;

public class MessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.message_root, new MessageFragment())
                .commit();
    }
}
