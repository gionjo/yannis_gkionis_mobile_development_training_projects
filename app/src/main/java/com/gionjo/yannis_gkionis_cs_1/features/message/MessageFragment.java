package com.gionjo.yannis_gkionis_cs_1.features.message;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gionjo.yannis_gkionis_cs_1.R;

public class MessageFragment extends Fragment {

    EditText messageEditText;
    TextView messageOutputTextView;

    public MessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_message, container, false);

        messageEditText = view.findViewById(R.id.typed_message);
        messageOutputTextView = view.findViewById(R.id.final_message);

        Button sendMessageButton = view.findViewById(R.id.send_msg_button);
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageOutputTextView.setText(messageEditText.getText().toString());
                messageEditText.setText("");
                hide_keyboard();
            }

            public void hide_keyboard() {
                InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(messageEditText.getWindowToken(), 0);
            }
        });





        return view;
    }

}