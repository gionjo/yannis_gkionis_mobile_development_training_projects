package com.gionjo.yannis_gkionis_cs_1.features.login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;


import com.gionjo.yannis_gkionis_cs_1.R;
import com.gionjo.yannis_gkionis_cs_1.features.profile.ProfileActivity;
import com.gionjo.yannis_gkionis_cs_1.features.rv.MockedProfileActivity;


public class LoginFragment extends Fragment {

    EditText usernameEditText,passwordEditText;
    Button loginButton;

    final String USERNAME = "yannis";
    final String PASSWORD = "777";


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        usernameEditText = view.findViewById(R.id.username_edittext);
        passwordEditText = view.findViewById(R.id.password_edittext);
        loginButton = view.findViewById(R.id.login_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (credentialsCorrect()){
                    Intent intent = new Intent(getActivity(),MockedProfileActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getActivity(),R.string.wrong_credentials_message,Toast.LENGTH_LONG).show();
                }

            }
        });



        return view;
    }

    private boolean credentialsCorrect() {
        return usernameEditText.getText().toString().equals(USERNAME) && passwordEditText.getText().toString().equals(PASSWORD);
    }

}
