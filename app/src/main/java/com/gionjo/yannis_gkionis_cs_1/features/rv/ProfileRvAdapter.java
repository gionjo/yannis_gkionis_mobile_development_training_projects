package com.gionjo.yannis_gkionis_cs_1.features.rv;

import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gionjo.yannis_gkionis_cs_1.R;

import java.util.ArrayList;

public class ProfileRvAdapter extends RecyclerView.Adapter<ProfileRvAdapter.ProfileViewHolder> {

    private ArrayList<Profile> profiles;

    public ProfileRvAdapter(ArrayList<Profile> profiles) {
        this.profiles = profiles;
    }

    public static class ProfileViewHolder extends RecyclerView.ViewHolder{
        TextView pName;
        TextView pSurname;
        TextView pAddress;
        TextView pPhoneNumber;
        TextView pEmail;

        public ProfileViewHolder(View v){

            super(v);
            pName = v.findViewById(R.id.profile_name);
            pSurname = v.findViewById(R.id.profile_surname);
            pAddress = v.findViewById(R.id.profile_address);
            pPhoneNumber = v.findViewById(R.id.profile_phonenumber);
            pEmail = v.findViewById(R.id.profile_email);

        }
    }


    @NonNull
    @Override
    public ProfileViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_mocked_profile_item,viewGroup,false);
        ProfileRvAdapter.ProfileViewHolder vh = new ProfileRvAdapter.ProfileViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileViewHolder viewHolder, int i){
        viewHolder.pName.setText(profiles.get(i).getName());
        viewHolder.pSurname.setText(profiles.get(i).getSurname());
        viewHolder.pAddress.setText(profiles.get(i).getAddress());
        viewHolder.pPhoneNumber.setText(profiles.get(i).getPhoneNumber());
        viewHolder.pEmail.setText(profiles.get(i).getEmail());
    }

    @Override
    public int getItemCount() {
        return profiles.size();
    }

}
