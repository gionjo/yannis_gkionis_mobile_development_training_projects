package com.gionjo.yannis_gkionis_cs_1.features.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gionjo.yannis_gkionis_cs_1.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.login_root,new LoginFragment())
                .commit();
    }
}
