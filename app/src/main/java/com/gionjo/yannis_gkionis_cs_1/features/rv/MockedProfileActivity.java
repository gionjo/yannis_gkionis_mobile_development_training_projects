package com.gionjo.yannis_gkionis_cs_1.features.rv;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.gionjo.yannis_gkionis_cs_1.R;
import com.gionjo.yannis_gkionis_cs_1.features.profile.ProfileFragment;

public class MockedProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mocked_profile);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mocked_profile_root,new MockedProfileFragment())
                .commit();
    }
}
