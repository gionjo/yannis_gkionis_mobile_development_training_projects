package com.gionjo.yannis_gkionis_cs_1.features.rv;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gionjo.yannis_gkionis_cs_1.R;
import com.gionjo.yannis_gkionis_cs_1.features.message.MessageActivity;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MockedProfileFragment extends Fragment {

    RecyclerView profilesRv;

    public MockedProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mocked_profile, container, false);

        profilesRv = v.findViewById(R.id.mocked_profile_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        profilesRv.setLayoutManager(layoutManager);

        ArrayList<Profile> profiles = new ArrayList<Profile>();
        insertMockProfiles(profiles);

        ProfileRvAdapter profilesRvAdapter = new ProfileRvAdapter(profiles);
        profilesRv.setAdapter(profilesRvAdapter);


        final Button sendMessageButton = v.findViewById(R.id.send_message_button);
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),MessageActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

    private void insertMockProfiles(ArrayList<Profile> profiles) {
        profiles.add(new Profile("Γιαννης", "Γκιώνης", "Υψηλάντου 27, Λουτράκι", "6906015600",
                "gionjo@gmail.com"));


        for (int i = 1; i < 10; i++) {
            Profile profile = new Profile("Name" + i, "Surname" + i, "Address" + i, "phone number" + i,
                    "email" + i);
            profiles.add(profile);
        }

    }
}
